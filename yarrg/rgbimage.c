/*
 * Handling of colour (RGB) images
 */
/*
 *  This is part of ypp-sc-tools, a set of third-party tools for assisting
 *  players of Yohoho Puzzle Pirates.
 * 
 *  Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 *  are used without permission.  This program is not endorsed or
 *  sponsored by Three Rings.
 */

#include "convert.h"

/*
 *
 *
 * widgets.txt
 * format
 *
 *  <magic>
 *
 *  <context>
 *  <string>
 *  ppmnoraw depth 8
 *
 *  <context>
 *  <string>
 *  ppmnoraw depth 8
 *  .
 *
 */

#include "convert.h"

static int identify1(const RgbImage *base, Rect portion,
		    char result[MAXIMGIDENT], const char *what,
		    const char *which, int (*opener)(const char *fn)) {
  char *dbfile_name= masprintf("_%s-pixmap.txt",which);
  if (!opener(dbfile_name))
    goto not_found;

#define FGETSLINE (dbfile_getsline(result,MAXIMGIDENT,__FILE__,__LINE__))

  FGETSLINE;
  dbassert(!strcmp(result,"# ypp-sc-tools pctb pixmaps v1"));
  
  void *row= 0;
  size_t rowa= 0;
  
  for (;;) {
    FGETSLINE;
    if (!result[0] || result[0]=='#') continue;
    if (!strcmp(result,".")) break;

    char magic[10];
    dbfile_getsline(magic,sizeof(magic),__FILE__,__LINE__);

    dbassert(!strcmp(magic,"P3"));
    int w,h,maxval;
    dbassert( dbfile_scanf("%d %d %d",&w,&h,&maxval) == 3);
    dbassert(w>0); dbassert(h>0); dbassert(maxval==255);

    if (rowa < w) {
      rowa= w;
      row= mrealloc(row, rowa*3);
    }
    int diff= w != RECT_W(portion) || h != RECT_H(portion);

    int x,y,i;
    for (y=0; y<h; y++) {
      for (x=0; x<w; x++) {
	uint32_t rgb=0;
	for (i=0; i<3; i++) {
	  int c;
	  dbassert( dbfile_scanf("%d",&c) == 1);
	  dbassert(c>=0 && c<=255);
	  rgb |= (Rgb)c << (i*8);
	}
	int px= portion.tl.x + x, py= portion.tl.y + y;
	diff |= px > portion.br.x || py > portion.br.y ||
	        rgb != ri_rgb(base,px,py);
      }
    }
    if (!diff) {
      progress_log("Identified %s image: %s.",what,result);
      goto found;
    }
  }
 not_found:
  result[0]= 0;

 found:
  dbfile_close();
  free(dbfile_name);
  return !!result[0];
}

static int identify(const RgbImage *base, Rect portion,
		    char result[MAXIMGIDENT], const char *what) {
  return identify1(base,portion,result,what, "master", dbfile_gzopen) ||
         identify1(base,portion,result,what, "local",  dbfile_open);
}

void fwrite_ppmraw(FILE *f, const RgbImage *ri) {
  int i;
  fprintf(f,
	  "P6\n"
	  "%d %d\n"
	  "255\n", ri->w, ri->h);
  for (i=0; i < ri->w * ri->h; i++) {
    Rgb rgb= ri->data[i];
    fputc_unlocked(rgb >> 0,  f);
    fputc_unlocked(rgb >> 8,  f);
    fputc_unlocked(rgb >> 16, f);
  }
  sysassert(!ferror(f));
  sysassert(!fflush(f));
}

static void fwrite_ppm(FILE *f, const RgbImage *base, Rect portion) {
  int x,y,i;
  fprintf(f,"P3\n%d %d\n255\n", RECT_W(portion), RECT_H(portion));
  for (y=portion.tl.y; y<=portion.br.y; y++) {
    for (x=portion.tl.x; x<=portion.br.x; x++) {
      putc(' ',f);
      Rgb rgb= ri_rgb(base,x,y);
      for (i=0; i<3; i++)
	fprintf(f," %3d", (rgb>>(i*8)) & 0xff);
    }
    putc('\n',f);
  }
  sysassert(!ferror(f));
  sysassert(!fflush(f));
}

void identify_rgbimage(const RgbImage *base, Rect portion,
		       char result[MAXIMGIDENT], const char *what) {
  static int synced;

  if (!synced) {
    if (o_flags & ff_dict_fetch)
      fetch_with_rsync_gz("pixmap");
    synced++;
  }
  
  for (;;) {
    int ok= identify(base, portion, result, what);
    if (ok) return;
    
    if (DEBUGP(pixmap))
      fwrite_ppm(stderr,base,portion);

    FILE *resolver= resolve_start();
    if (!resolver)
	fatal("Image recognition failed - unrecognised island.\n"
	      "See FIXME.FIXME\n");

    fprintf(resolver, "pixmap\n" "%s\n", what);
    fwrite_ppm(resolver, base, portion);
    putc('\n',resolver);

    resolve_finish();
  }
}

RgbImage *alloc_rgb_image(int w, int h) {
  RgbImage *ri;
  ri= mmalloc(sizeof(*ri) + w*h*sizeof(ri->data[0]));
  ri->w= w;
  ri->h= h;
  return ri;
}
