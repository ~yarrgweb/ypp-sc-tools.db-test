<%doc>

 This is part of the YARRG website.  YARRG is a tool and website
 for assisting players of Yohoho Puzzle Pirates.

 Copyright (C) 2009 Ian Jackson <ijackson@chiark.greenend.org.uk>
 Copyright (C) 2009 Clare Boothby

  YARRG's client code etc. is covered by the ordinary GNU GPL (v3 or later).
  The YARRG website is covered by the GNU Affero GPL v3 or later, which
   basically means that every installation of the website will let you
   download the source.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 Yohoho and Puzzle Pirates are probably trademarks of Three Rings and
 are used without permission.  This program is not endorsed or
 sponsored by Three Rings.


 This Mason component generates information about the uploader.


</%doc>
<& docshead &>
<%shared>
my $url_base_base= 'http://yarrg.chiark.net/download/jarrg';
</%shared>
<%def downloadurl><%args>
$leaf
$test => 0
#        ^ change this to change the default version
</%args><%perl>
my $download_version= $test ? '/test' : '';
my $url_base= "$url_base_base$download_version";
my $url= $url_base.'/'.$leaf;
</%perl><a href="<% $url %>"><%
    $m->has_content ? $m->content : $url
%></a></%def>
<h1>Uploading to YARRG</h1>
<p>

The YARRG system has two main parts: this website which maintains a
searchable database of commodity prices, and an upload client (known
as JARRG), which screenscrapes the commodity data from the
Puzzle Pirates game client and uploads it to the database.

<p>

You will need to upload data yourself if there is no data for your
ocean or island, or the data that is there is too old.  You should
also upload when preparing a trade run, as market conditions change and
you want a plan based on recent data.

<p>

The JARRG upload client uploads both to YARRG and
<a href="http://pctb.crabdance.com/">PCTB</a>.  It is intended to
replace both the <a href="scraper">"yarrg"</a> and "PPAOCR" screenscrapers.

<h2>Instructions</h2>

First you must install JARRG, following the instructions for
your operating system, below.  This will not modify your Puzzle
Pirates installation.  Instead, it provides you with a new way to run
Puzzle Pirates which integrates it with JARRG to provides a YARRG/PCTB
Upload facility.

<p>

To upload data, run the Puzzle Pirates client with JARRG (as detailed
below).  You should see both the Puzzle Pirates window and the JARRG
window appear.  Log in normally, and visit a commodity trading screen
(at a market, shoppe or vessel hold).  Select "All Commodities" in the
YPP client.  Then press "Upload Market Data" in the JARRG window.

<p>

It is OK to switch away from the commodity list in Puzzle Pirates as
soon as the Jarrg progress bar appears.  The uploader has taken a copy
of the data, so you can do something else while the upload takes
place.

<p>

The JARRG integration is done via fully supported Java Accessibility
interfaces, and should not disturb the normal running of Puzzle
Pirates.  However, please make sure that if your YPP client misbehaves
at all, you try starting it in the vanilla way (without JARRG
integration) before asking for help.  In particular, if the JARRG
startup does not work, but the ordinary Puzzle Pirates startup does,
report the problem to us, not to Three Rings.

<h3>Installing and running on Linux</h3>

Download
<& downloadurl, leaf => 'jarrg-linux.tar.gz' &>
and unpack it.  You run jarrg from the command line.  Change
(<code>cd</code>) to the top level <code>jarrg</code> directory
which was created when you unpacked the tarball, and then run
<pre>
./jarrg /path/to/yohoho/yohoho
</pre>
where <code>/path/to/yohoho/yohoho</code> is the location of the
ordinary Puzzle Pirates startup script, which is normally
<code>yohoho/yohoho</code> in your home directory.

<h3>Installing on Windows</h3>

Download
<& downloadurl, leaf => 'jarrg-setup.exe' &>
and double-click on it.  It will either:
<ol>
<li>Just work, in which case you'll have a new icon on your desktop which
   runs Puzzle Pirates with JARRG integrated.
<li>Fail, and tell you what to do next.  Usually this means installing a
   Java Runtime Environment (or JVM) and then uninstalling and re-
   installing Puzzle Pirates.  (Don't worry about reinstalling; you
   won't lose any of your pirates or pieces of eight!)
</ol>

<h3>Installing on Macs</h3>

We believe that it should be straightforward for a MacOS expert to get
JARRG working properly on MacOS but we have not been able to
test this ourselves.  The Linux installation method is probably the
best starting point.

<h2>How does it work?  Is it a violation of the Terms of Service?</h2>

JARRG (and the old OCR clients) comply with Three Rings' official
<a href="http://yppedia.puzzlepirates.com/Official:Third_Party_Software">Third Party Software Policy</a>.

<p>

Essentially, JARRG is a specialised "screen reader" which instead of
reading information out loud, uploads it to the YARRG and PCTB
databases.

<p>

JARRG uses the Java Accessibility API, which is a part of the
Java platform.  It's a facility in Java, available for all Java
programs, intended to help make applications available to users with
disabilities: for example, it permits hooking in screen readers.  The
use of the Java Accessibility API was helpfully suggested by a Ringer
in the forums (to another developer of a third-party tool, wanting
help getting information out of Duty Reports).

<p>

Installing JARRG does not modify any game files, and does not
hook into the game itself.  It makes no permanent or global changes to
your overall computer, operating system or Java configuration.  And it
does not access (indeed, we don't think it could access) any of the
core game functionality which would make cheating possible.

<p>

JARRG runs the unmodified Puzzle Pirates game, but with a separate
copy of your system's JVM (Java Virtual Machine); that copy of the JVM
is configured to use the JARRG accessibility plugin.  We do this
(rather than configuring the computer's main JVM to use the
accessibility plugin) to avoid interfering with other uses of Java on
your computer - in particular, so that it is always possible to launch
Puzzle Pirates <em>without</em> JARRG (for example, in case the plugin
should cause any kind of problem).

<h2><a name="source">Authorship, source code and other versions</a></h2>

Thanks to Burninator for writing the core of the JARRG client.  Ian
Jackson and Owen Dunn adapted it to fix bugs and to improve the
installation setup (in particular, to avoid modifying any of the YPP
client's startup files).  Owen Dunn added support for uploading to
YARRG, updated the build system, and wrote a Windows installer.

<p>

JARRG is
Copyright 2006-2009 Burninator,
Copyright 2009-2010 Owen Dunn and
Copyright 2009-2011 Ian Jackson.
It is Free Software with <strong>NO WARRANTY</strong>, released under
the MIT-style two-clause licence.

<p>

The code for the downloadable binaries is in
in <& downloadurl, leaf=>'jarrg-source.tar.gz' &>.
The download directory also sometimes contains other versions
(eg unreleased test versions), which you can see here:
 <& downloadurl, leaf => '', test => 0 &>

<p>

We maintain JARRG in git, and you can get
source code for recent and perhaps unreleased versions from one of:
<pre>
git://git.yarrg.chiark.net/jarrg-ian.git</code> <a href="http://www.chiark.greenend.org.uk/ucgi/~yarrgweb/git?p=jarrg-ian.git;a=summary">(gitweb)</a>
git://git.yarrg.chiark.net/jarrg-owen.git</code>&nbsp;<a href="http://www.chiark.greenend.org.uk/ucgi/~yarrgweb/git?p=jarrg-owen.git;a=summary">(gitweb)</a>
</pre>


</div>
<& footer &>
